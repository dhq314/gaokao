# -*- coding:utf-8 -*-
"""
工具函数
Created on 2015/11/27
@author: Joe Deng
@contact: dhq314@gmail.com
"""

import os
import time
import json
import urlparse
import tushare as ts
import requests

import constant
import mysql


def ceil2(x, y):
    """
    Python2 的除数向上取整
    :param x: 除数
    :param y: 被除数
    :return: 向上取整的值
    """
    (div, mod) = divmod(x, y)
    if mod > 0:
        return div + 1
    else:
        return div


def parse_qs(url):
    """
    获取 URL 参数
    :return: dict
    """
    query = urlparse.urlparse(url).query
    return dict([(k, v[0]) for k, v in urlparse.parse_qs(query).items()])


def get_xueqiu_session():
    """
    获取雪球网会话
    :return:
    """
    session = requests.Session()
    session.headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
    }
    session.get('https://xueqiu.com/')
    return session


def page(cur_page, per_page, total_num):
    """
    分页函数
    """
    total_page = ceil2(total_num, per_page)
    cur_page = cur_page.strip()
    if cur_page.isdigit():
        cur_page = int(cur_page)
        if cur_page < 1:
            cur_page = 1
    else:
        cur_page = 1
    prev_page = cur_page - 1
    if prev_page < 1:
        prev_page = 1
    next_page = cur_page + 1
    if next_page > total_page:
        next_page = total_page
    offset = (cur_page - 1) * per_page
    range_page = 4
    show_items = (range_page * 2) + 1
    return cur_page, total_num, total_page, prev_page, next_page, offset, range_page, show_items


def get_config_data():
    """
    获取配置数据
    :return: config_data
    """
    config_file = os.path.join(os.path.dirname(__file__), 'config.json')
    with open(config_file, 'rb') as fp:
        config_data = json.load(fp)
    return config_data


def get_current_code(code):
    """
    获取当前交易数据
    http://tushare.org/trading.html#id6
    :param code:
    :return:
    """
    try:
        df = ts.get_realtime_quotes(code)
        return df.iloc[0]
    except Exception as e:
        print e
        return {"code": code, "price": 0, "pre_close": 0}


def get_stock_symbol(code):
    """
    加上交易所前缀
    :param code:
    :return:
    """
    return 'sh' + code if code[:1] == '6' else 'sz' + code


def format_to_seconds(string, time_format=None):
    """
    返回时间格式的时间戳（2017-05-19 00:00:00 -> 1495123200）
    :param string:
    :param time_format:
    :return:
    """
    if time_format is None:
        time_format = '%Y-%m-%d %H:%M:%S'
    string = str(string)
    return int(time.mktime(time.strptime(string, time_format)))


def seconds_to_format(seconds, time_format=None):
    """
    时间戳返回时间格式（1495123200 -> 2017-05-19 00:00:00）
    :param seconds:
    :param time_format:
    :return:
    """
    if time_format is None:
        time_format = '%Y-%m-%d %H:%M:%S'
    return time.strftime(time_format, time.localtime(seconds))


def get_milli_second():
    """
    获取毫秒
    :return:
    """
    return int(time.time() * 1000)


def is_same_date(seconds1, seconds2):
    """
    是否是同一天
    :param seconds1:
    :param seconds2:
    :return:
    """
    date1 = seconds_to_format(seconds1, '%Y-%m-%d')
    date2 = seconds_to_format(seconds2, '%Y-%m-%d')
    return date1 == date2


def get_config(key):
    """
    获取配置数据
    :param key:
    :return:
    """
    db = mysql.Mysql()
    val = db.fetch_one(constant.TAB_CONFIG, "val", "`key` = '%s'" % key)
    db.close()
    return val


def update_config(key, val):
    """
    更新配置数据
    :param key:
    :param val:
    :return:
    """
    db = mysql.Mysql()
    insert_dict = {
        'key': key,
        'val': val
    }
    db.replace(constant.TAB_CONFIG, insert_dict)
    db.close()
    return True
