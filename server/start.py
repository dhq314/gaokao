#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import tornado.options
import tornado.web
import tornado.ioloop
from tornado.escape import json_encode

import constant
import util
import mysql


class SchoolHandler(tornado.web.RequestHandler):
    def get(self):
        query_dict = util.parse_qs(self.request.uri)
        action = query_dict["action"]
        db = mysql.Mysql()
        ret = 1
        if action == "school_year":
            year = int(query_dict["year"])
            d = db.fetch_one(constant.TAB_SCHOOL_YEAR, "year", "year = %d" % year)
            if d is None:
                insert_dict = {'year': year}
                db.insert(constant.TAB_SCHOOL_YEAR, insert_dict)
        elif action == "obtain_school_year":
            d = db.fetch_one(constant.TAB_SCHOOL_YEAR, 'year', "is_finish = 0 ORDER BY `year` DESC LIMIT 1")
            ret = 0
            if d is not None:
                ret = d["year"]
        elif action == "school_gear":
            year = int(query_dict["year"])
            gear = str(query_dict["gear"])
            d = db.fetch_one(constant.TAB_SCHOOL_GEAR, "gear", "year = %d AND gear = '%s'" % (year, gear))
            if d is None:
                gear_name = str(query_dict["gear_name"])
                insert_dict = {'year': year, 'gear': gear, 'gear_name': gear_name}
                db.insert(constant.TAB_SCHOOL_GEAR, insert_dict)
        elif action == "obtain_school_gear":
            year = int(query_dict["year"])
            d = db.fetch_one(constant.TAB_SCHOOL_GEAR, 'gear',
                             "year = %d AND is_finish = 0 ORDER BY `gear` ASC LIMIT 1" % year)
            ret = ""
            if d is not None:
                ret = d["gear"]
        elif action == "school_area":
            year = int(query_dict["year"])
            gear = str(query_dict["gear"])
            area = int(query_dict["area"])
            d = db.fetch_one(constant.TAB_SCHOOL_AREA, "area",
                             "year = %d AND gear = '%s' AND area = %d" % (year, gear, area))
            if d is None:
                area_name = str(query_dict["area_name"])
                insert_dict = {'year': year, 'gear': gear, 'area': area, 'area_name': area_name}
                db.insert(constant.TAB_SCHOOL_AREA, insert_dict)
        elif action == "obtain_school_area":
            year = int(query_dict["year"])
            gear = str(query_dict["gear"])
            d = db.fetch_one(constant.TAB_SCHOOL_AREA, 'area',
                             "year = %d AND gear = '%s' AND is_finish = 0 ORDER BY `area` ASC LIMIT 1" % (
                                 year, gear))
            ret = 0
            if d is not None:
                ret = d["area"]
        elif action == "update_school":
            d = {
                'year': query_dict["year"],
                'gear': query_dict["gear"],
                'area': query_dict["area"],
                'school_id': query_dict["school_id"],
                'school_name': query_dict["school_name"],
                'school_url': query_dict["school_url"],
                'group': query_dict["group"],
                'plan': query_dict["plan"],
                'cast': query_dict["cast"],
                'accept': query_dict["accept"],
                'max_rank': query_dict["max_rank"],
                'average_rank': query_dict["average_rank"],
                'min_rank': query_dict["min_rank"]
            }
            db.replace(constant.TAB_SCHOOL, d)
        elif action == "mark_year_finish":  # 标记年度条件完成
            year = int(query_dict["year"])
            db.update(constant.TAB_SCHOOL_YEAR, {'is_finish': 1}, "year = %d" % year)
        elif action == "mark_gear_finish":  # 标记录取批次条件完成
            year = int(query_dict["year"])
            gear = str(query_dict["gear"])
            db.update(constant.TAB_SCHOOL_GEAR, {'is_finish': 1}, "year = %d AND gear = '%s'" % (year, gear))
        elif action == "mark_area_finish":  # 标记省市条件完成
            year = int(query_dict["year"])
            gear = str(query_dict["gear"])
            area = int(query_dict["area"])
            db.update(constant.TAB_SCHOOL_AREA, {'is_finish': 1},
                      "year = %d AND gear = '%s' AND area = %d" % (year, gear, area))
        elif action == "obtain_open_sta":  # 获取爬取的开关状态
            ret = 0
            rs = util.get_config(constant.KEY_OPEN_STA)
            if rs is not None:
                if rs["val"] == "1":
                    ret = 1
        elif action == "trigger_open_sta":  # 改变爬取的开关状态
            ret = 0
            rs = util.get_config(constant.KEY_OPEN_STA)
            if rs is not None:
                if rs["val"] == "1":
                    ret = 1
            ret = 1 if ret == 0 else 0
            util.update_config(constant.KEY_OPEN_STA, ret)

        db.close()
        self.write(str(ret))


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        query_dict = util.parse_qs(self.request.uri)
        action = query_dict["action"]
        db = mysql.Mysql()
        if action == "fetch_gear":
            year = int(query_dict["year"])
            data = db.fetch_all(constant.TAB_SCHOOL_GEAR, "gear, gear_name", "year = %d ORDER BY `gear` ASC" % year)
        elif action == "fetch_area":
            year = int(query_dict["year"])
            gear = int(query_dict["gear"])
            data = db.fetch_all(constant.TAB_SCHOOL_AREA, "area, area_name",
                                "year = %d AND gear = '%s' ORDER BY `area` ASC" % (year, gear))
        elif action == "fetch_year":
            data = db.fetch_all(constant.TAB_SCHOOL_YEAR, "year", "1 ORDER BY `year` DESC")
        else:
            year = int(query_dict["year"])
            gear = int(query_dict["gear"])
            area = int(query_dict["area"])
            data = db.fetch_all(constant.TAB_SCHOOL, "*",
                                "year = %d AND gear = '%s' AND area = %d" % (year, gear, area))
        db.close()
        self.write(json_encode({"data": data}))


if __name__ == "__main__":
    if sys.getdefaultencoding() != 'gbk':
        reload(sys)
        sys.setdefaultencoding("utf-8")
    WEB_NAME = "GaoKao"
    WEB_SERVER_LISTEN_PORT = 3522

    tornado.options.parse_command_line()
    handlers = [
        (r"/school", SchoolHandler),
        (r"/", MainHandler),
    ]
    root_path = os.path.dirname(__file__)
    settings = dict(
        title=WEB_NAME,
        template_path=os.path.join(root_path, "templates"),
        static_path=os.path.join(root_path, "static")
    )
    application = tornado.web.Application(handlers, debug=True, **settings)
    application.listen(WEB_SERVER_LISTEN_PORT)
    print "Tornado Version %s" % tornado.version
    print "%s Server Launched, Listen Port %d..." % (WEB_NAME, WEB_SERVER_LISTEN_PORT)
    tornado.ioloop.IOLoop.instance().start()
