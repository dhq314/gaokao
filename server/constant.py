#!/usr/bin/env python
# -*- coding: utf-8 -*-

TAB_CONFIG = "config"

TAB_SCHOOL = "school"
TAB_SCHOOL_YEAR = "school_year"
TAB_SCHOOL_GEAR = "school_gear"
TAB_SCHOOL_AREA = "school_area"

# 爬取的开关状态
KEY_OPEN_STA = "key_open_state"
