#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb
import itertools
import util


class Mysql(object):
    """docstring for mysql"""

    def __init__(self):
        config = util.get_config_data()
        self.host = config['db_host']
        self.port = config['db_port']
        self.user = config['db_user']
        self.password = config['db_password']
        self.name = config['db_name']
        self.charset = config['db_charset']
        self._conn = None
        self._connect()
        self._cursor = self._conn.cursor()
        # Turn off MySQLdb's warning raising
        self._cursor._defer_warnings = True

    def _connect(self):
        try:
            self._conn = MySQLdb.connect(
                host=self.host, port=self.port,
                user=self.user,
                passwd=self.password,
                db=self.name,
                charset=self.charset)
        except MySQLdb.Error, e:
            print e

    def query(self, sql):
        try:
            result = self._cursor.execute(sql)
        except MySQLdb.Error, e:
            print e
            result = False
        return result

    def fetch_all(self, table, column='*', condition=''):
        condition = ' WHERE ' + condition if condition else None
        if condition:
            sql = "SELECT %s FROM %s %s" % (column, table, condition)
        else:
            sql = "SELECT %s FROM %s" % (column, table)
        return self.fetch_sql(sql)

    def fetch_one(self, *parameters):
        rows = self.fetch_all(*parameters)
        if not rows:
            return None
        elif len(rows) > 1:
            raise Exception("Multiple rows returned for Database.get() query")
        else:
            return rows[0]

    def fetch_sql(self, sql):
        """
        通过SQL直接获取数据
        :param sql:
        :return:
        """
        self.query(sql)
        column_names = [d[0] for d in self._cursor.description]
        return [Row(itertools.izip(column_names, row)) for row in self._cursor]

    def insert(self, table, insert_dict):
        column = ''
        value = ''
        for key in insert_dict:
            column += ',`' + key + '`'
            value += "','" + str(insert_dict[key])
        column = column[1:]
        value = value[2:] + "'"
        sql = "INSERT INTO %s (%s) VALUES (%s)" % (table, column, value)
        self._cursor.execute(sql)
        self._conn.commit()
        return self._cursor.lastrowid  # 返回最后的id

    def replace(self, table, insert_dict):
        column = ''
        value = ''
        for key in insert_dict:
            column += ',`' + key + '`'
            value += "','" + str(insert_dict[key])
        column = column[1:]
        value = value[2:] + "'"
        sql = "REPLACE INTO %s (%s) VALUES (%s)" % (table, column, value)
        self._cursor.execute(sql)
        self._conn.commit()
        return self._cursor.lastrowid  # 返回最后的id

    def update(self, table, up_dict, condition=''):
        """
        更新语句
        :param table:
        :param up_dict:
        :param condition:
        :return:
        """
        if not condition:
            print "must have id"
            exit()
        else:
            condition = 'WHERE ' + condition
        value = ''
        for key in up_dict:
            value += ",%s='%s'" % (key, up_dict[key])
        value = value[1:]
        sql = "UPDATE %s SET %s %s" % (table, value, condition)
        self._cursor.execute(sql)
        self._conn.commit()
        return self.affected_num()

    def delete(self, table, condition=''):
        """
        删除表数据
        :param table:
        :param condition:
        :return:
        """
        condition = 'where ' + condition if condition else None
        sql = "DELETE FROM %s %s" % (table, condition)
        self._cursor.execute(sql)
        self._conn.commit()
        return self.affected_num()

    def truncate(self, table):
        """
        清空表数据
        :param table:
        :return:
        """
        sql = "TRUNCATE TABLE %s" % table
        self.query(sql)

    def drop(self, table):
        """
        删除表
        :param table:
        :return:
        """
        sql = "DROP TABLE IF EXISTS `%s`" % table
        self.query(sql)

    def rollback(self):
        self._conn.rollback()

    def affected_num(self):
        """
        返回受影响行数
        :return:
        """
        return self._cursor.rowcount

    def get_all_table(self):
        self._cursor.execute("SHOW TABLES")
        table_tuple = self._cursor.fetchall()
        table_list = []
        for tab in table_tuple:
            table_list.append(tab[0])
        return table_list

    def __del__(self):
        try:
            self._cursor.close()
            self._conn.close()
        except:
            pass

    def close(self):
        self.__del__()


class Row(dict):
    """A dict that allows for object-like property access syntax."""

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

# if __name__ == '__main__':
#     db = Mysql()
#
#     print db.select('msg', 'id,ip,domain')
#     print db.select('msg', 'id,ip,domain', 'id>2')
#     print db.affected_num()
#
#     tdict = {
#         'ip': '111.13.100.91',
#         'domain': 'baidu.com'
#     }
#     print db.insert('msg', tdict)
#
#     tdict = {
#         'ip': '111.13.100.91',
#         'domain': 'aaaaa.com'
#     }
#     print db.update('msg', tdict, 'id=5')
#
#     print db.delete('msg', 'id>3')
#
#     db.close()
