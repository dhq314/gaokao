$(document).ready(function () {
    var GaoKao = {
        is_handling: false,
        url: "http://" + Config.domain + ":3522/",

        fetch_year: function () {
            $.getJSON(GaoKao.url, {action: "fetch_year"}, function (j) {
                var _html = [];
                _html.push('<option value="0">招生年度</option>');
                $.each(j.data, function (index, item) {
                    _html.push('<option value="', item.year, '">', item.year, '年</option>');
                });
                var year_obj = $("#year");
                year_obj.html(_html.join(""));
                year_obj.change(function () {
                    GaoKao.fetch_gear();
                });
            });
        },

        fetch_gear: function () {
            var year = $("#year").val();
            var data = {action: "fetch_gear", year: year};
            $.getJSON(GaoKao.url, data, function (j) {
                var _html = [];
                _html.push('<option value="0">录取批次</option>');
                $.each(j.data, function (index, item) {
                    _html.push('<option value="', item.gear, '">', item.gear_name, '</option>');
                });
                var gear_obj = $("#gear");
                gear_obj.html(_html.join(""));
                gear_obj.change(function () {
                    GaoKao.fetch_area();
                });
            });
        },

        fetch_area: function () {
            var year = $("#year").val();
            var gear = $("#gear").val();
            var data = {action: "fetch_area", year: year, gear: gear};
            $.getJSON(GaoKao.url, data, function (j) {
                var _html = [];
                _html.push('<option value="0">省市</option>');
                $.each(j.data, function (index, item) {
                    _html.push('<option value="', item.area, '">', item.area_name, '</option>');
                });
                var area_obj = $("#area");
                area_obj.html(_html.join(""));
                area_obj.change(function () {
                    GaoKao.fetch_school_data();
                });
            });
        },

        fetch_school_data: function () {
            var year = $("#year").val();
            var gear = $("#gear").val();
            var area = $("#area").val();
            if (year > 0 && gear !== "" && area > 0) {
                var _html = [];
                _html.push('<table class="table table-bordered table-hover table-striped text-center">');
                _html.push('<thead>');
                _html.push('<tr>');
                _html.push('<td>年份</td>');
                _html.push('<td>院校名称</td>');
                _html.push('<td>官网</td>');
                _html.push('<td>科目组</td>');
                _html.push('<td>计划数</td>');
                _html.push('<td>投档数</td>');
                _html.push('<td>录取数</td>');
                _html.push('<td>最高名次</td>');
                _html.push('<td>平均名次</td>');
                _html.push('<td>最低名次</td>');
                _html.push('</tr>');
                _html.push('</thead>');
                _html.push('<tbody id="school_data">');
                _html.push('<tr>');
                _html.push('<td colspan="10">Loading....</td>');
                _html.push('</tr>');
                _html.push('</tbody>');
                _html.push('</table>');
                $("#school_table").html(_html.join(""));
                var data = {action: "school_data", year: year, gear: gear, area: area};
                $.getJSON(GaoKao.url, data, function (j) {
                    var _html = [];
                    $.each(j.data, function (index, item) {
                        _html.push('<tr>');
                        _html.push('<td>', item.year, '</td>');
                        _html.push('<td>', item.school_name, '</td>');
                        _html.push('<td><a href="', item.school_url, '">官网</a></td>');
                        _html.push('<td>', item.group, '</td>');
                        _html.push('<td>', item.plan, '</td>');
                        _html.push('<td>', item.cast, '</td>');
                        _html.push('<td>', item.accept, '</td>');
                        _html.push('<td>', item.max_rank, '</td>');
                        _html.push('<td>', item.average_rank, '</td>');
                        _html.push('<td>', item.min_rank, '</td>');
                        _html.push('</tr>');
                    });
                    $("#school_data").html(_html.join(""));
                });
            }
        }
    };

    $.get(Config.school_url, {action: "obtain_open_sta"}, function (open_sta) {
        var open_text = open_sta === "1" ? "关闭爬取" : "打开爬取";
        var open_obj = $("#school-open");
        open_obj.text(open_text);
        open_obj.click(function () {
            if (GaoKao.is_handling) {
                return false;
            }
            GaoKao.is_handling = true;
            $.get(Config.school_url, {action: "trigger_open_sta"}, function (open_sta) {
                var open_text = open_sta === "1" ? "关闭爬取" : "打开爬取";
                open_obj.text(open_text);
                GaoKao.is_handling = false;
            });
            return false;
        });
    });

    $("#query-school").click(function () {
        GaoKao.fetch_school_data();
        return false;
    });

    GaoKao.fetch_year();
});
