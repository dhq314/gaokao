// $("#es_start").click(function () {
//
//     chrome.windows.getCurrent(function (currentWindow) {
//         //获取有指定属性的标签，为空获取全部标签
//         chrome.tabs.query({
//             active: true, windowId: currentWindow.id
//         }, function (activeTabs) {
//             console.log("TabId:" + activeTabs[0].id);
//             //执行注入，第一个参数表示向哪个标签注入
//             //allFrames表示如果页面中有iframe，是否也向iframe中注入脚本
//             chrome.tabs.executeScript(activeTabs[0].id, {file: "lib/jquery-2.0.1.js"}, function () {
//                 chrome.tabs.executeScript(activeTabs[0].id, {
//                     file: "js/action.js",
//                     allFrames: false
//                 });
//             });
//
//         });
//     });
//
// });

// http://dhq.me:3522/school?action=update_school&year=2016&gear=G&area=21&school_id=14472&school_name=%E6%B8%A4%E6%B5%B7%E7%90%86%E5%B7%A5%E8%81%8C%E4%B8%9A%E5%AD%A6%E9%99%A2&group=%E7%90%86%E5%B7%A5%E7%B1%BB&plan=15&cast=0&accept=0&max_rank=0&average_rank=0&min_rank=0

// truncate school;
// truncate school_area;
// truncate school_gear;
// truncate school_year;

/**
 * 获取对象长度
 * @returns {number}
 */
Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            size++;
        }
    }
    return size;
};


// 招生年度标识ID
var year_id = "ctl00_ContentPlaceHolder1_DropDownList_nian";
// 录取批次标识ID
var gear_id = "ctl00_ContentPlaceHolder1_DropDownList_pici";
// 省市标识ID
var area_id = "ctl00_ContentPlaceHolder1_DropDownList_sheng";
//数据表格的标识ID
var tab_id = "ctl00_ContentPlaceHolder1_UC_Luqu_Yuanxiao1_GridView1";
//当前页数
var current_page_id = "ctl00_ContentPlaceHolder1_UC_Luqu_Yuanxiao1_GridView1_ctl23_lblPageIndex";
//总页
var total_page_id = "ctl00_ContentPlaceHolder1_UC_Luqu_Yuanxiao1_GridView1_ctl23_lblPageCount";

var submit_id = "aspnetForm";

var KEY_YEAR = "gaokao_key_year";
var KEY_GEAR = "gaokao_key_gear";
var KEY_AREA = "gaokao_key_area";


var GaoKaoSchool = {

    /**
     * 获取院校数据
     */
    obtain_school_data: function () {
        var year_arr = GaoKaoSchool._get_year_arr();
        if (year_arr.length > 0) {
            var gear_obj = GaoKaoSchool._get_gear_arr();
            var gear_obj_size = Object.size(gear_obj);
            if (gear_obj_size > 0) {
                var area_obj = GaoKaoSchool._get_area_arr();
                var area_obj_size = Object.size(area_obj);
                if (area_obj_size > 0) {
                    if ($("#" + tab_id).length > 0) {           // 存在数据表格
                        var year = localStorage.getItem(KEY_YEAR);
                        var gear = localStorage.getItem(KEY_GEAR);
                        var area = localStorage.getItem(KEY_AREA);
                        $("#" + tab_id + " .GridView_RowStyle").each(function () {
                            var tds = $(this).children();
                            var school = $.trim($(tds[3]).text()).split(".");
                            var data = {
                                "action": "update_school",
                                "year": year,
                                "gear": gear,
                                "area": area,
                                "school_id": school[0],
                                "school_name": school[1],
                                "school_url": $(tds[4]).children(":first").attr("href"),
                                "group": $(tds[5]).text(),
                                "plan": $(tds[6]).text(),
                                "cast": $(tds[7]).text(),
                                "accept": $(tds[8]).text(),
                                "max_rank": $(tds[9]).text(),
                                "average_rank": $(tds[10]).text(),
                                "min_rank": $(tds[11]).text()
                            };
                            GaoKaoSchool.ajax_sync(data, function (rs) {
                                console.log(rs);
                            });
                            console.log(data);
                        });
                        if ($("#" + current_page_id).length > 0) {
                            var current_page = parseInt($("#" + current_page_id).text());
                            var total_page = parseInt($("#" + total_page_id).text());
                            console.log(current_page, total_page);
                            //有分页数据
                            if (current_page < total_page) {
                                //获取下一页数据
                                $("#ctl00_ContentPlaceHolder1_UC_Luqu_Yuanxiao1_GridView1_ctl23_txtNewPageIndex1").val(current_page + 1);
                                GaoKaoSchool.do_post("ctl00$ContentPlaceHolder1$UC_Luqu_Yuanxiao1$GridView1$ctl23$btnGo", "");
                            } else {
                                GaoKaoSchool.mark_area_finish(year, gear, area);
                            }
                        } else {
                            GaoKaoSchool.mark_area_finish(year, gear, area);
                        }
                    } else {
                        // 获取省市条件数据
                        GaoKaoSchool.obtain_school_area();
                    }
                } else {
                    // 获取录取批次条件数据
                    GaoKaoSchool.obtain_school_gear();
                }
            } else {
                // 初始招生年度条件数据
                GaoKaoSchool._init_school_year_condition();
                // 获取年度
                $.get(Config.school_url, {"action": "obtain_school_year"}, function (year) {
                    if (year > 0) {
                        localStorage.setItem(KEY_YEAR, year);
                        $("#" + year_id + " option[value=" + year + "]").attr('selected', 'selected'); // 1
                        GaoKaoSchool.do_post("ctl00$ContentPlaceHolder1$DropDownList_nian", "");
                    }
                });
            }
        }
    },

    /**
     * 招生年度数据
     * @private
     */
    _get_year_arr: function () {
        var year_arr = [];
        $('select#' + year_id + ' option').each(function () {
            var year = $(this).attr("value");
            if (year !== "0000") {
                year_arr.push(year);
            }
        });
        return year_arr;
    },

    /**
     * 初始招生年度条件数据
     * @private
     */
    _init_school_year_condition: function () {
        var year_arr = GaoKaoSchool._get_year_arr();
        if (year_arr.length > 0) {
            for (var i = 0; i < year_arr.length; i++) {
                var data = {"action": "school_year", "year": year_arr[i]};
                GaoKaoSchool.ajax_sync(data, function (rs) {
                    console.log(rs);
                });
            }
        }
    },

    /**
     * 获取录取批次条件数据
     */
    obtain_school_gear: function () {
        // 初始录取批次条件数据
        var year = localStorage.getItem(KEY_YEAR);
        GaoKaoSchool._init_school_gear_condition(year);
        // 获取录取批次
        var data = {
            "action": "obtain_school_gear",
            "year": year
        };
        $.get(Config.school_url, data, function (gear) {
            if (gear !== "") {
                localStorage.setItem(KEY_GEAR, gear);
                $("#" + gear_id + " option[value=" + gear + "]").attr('selected', 'selected');
                GaoKaoSchool.do_post("ctl00$ContentPlaceHolder1$DropDownList_pici", ""); // 2
            } else {
                GaoKaoSchool.mark_year_finish(year)
            }
        });
    },

    /**
     * 录取批次数据
     * @private
     */
    _get_gear_arr: function () {
        var gear_obj = {};
        $('select#' + gear_id + ' option').each(function () {
            var key = $(this).attr("value");
            if (key !== "#") {
                gear_obj[key] = $(this).text();
            }
        });
        return gear_obj;
    },

    /**
     * 初始录取批次条件数据
     * @private
     */
    _init_school_gear_condition: function (year) {
        var gear_obj = GaoKaoSchool._get_gear_arr();
        for (var gear in gear_obj) {
            if (gear_obj.hasOwnProperty(gear)) {
                var data = {
                    "action": "school_gear",
                    "year": year,
                    "gear": gear,
                    "gear_name": gear_obj[gear]
                };
                GaoKaoSchool.ajax_sync(data, function (rs) {
                    console.log(rs);
                });
            }
        }
    },

    /**
     * 获取省市条件数据
     */
    obtain_school_area: function () {
        // 初始省市条件数据
        var year = localStorage.getItem(KEY_YEAR);
        var gear = localStorage.getItem(KEY_GEAR);
        GaoKaoSchool._init_school_area_condition(year, gear);
        // 获取省市
        var data = {
            "action": "obtain_school_area",
            "year": year,
            "gear": gear
        };
        $.get(Config.school_url, data, function (area) {
            if (area > 0) {
                localStorage.setItem(KEY_AREA, area);
                $("#" + area_id + " option[value=" + area + "]").attr('selected', 'selected');
                // 勾选全部数据
                $("#ctl00_ContentPlaceHolder1_RadioButtonList_filter_0").attr('checked', true);
                // GaoKaoSchool.do_post("ctl00$ContentPlaceHolder1$RadioButtonList_filter$0", ""); // 3
                GaoKaoSchool.do_post("ctl00$ContentPlaceHolder1$Button_chaxun", ""); // 3
            } else {
                //标记录取批次条件完成
                GaoKaoSchool.mark_gear_finish(year, gear);
            }
        });
    },

    /**
     * 省市数据
     * @returns {{}}
     * @private
     */
    _get_area_arr: function () {
        var area_obj = {};
        $('#' + area_id + ' option').each(function () {
            var key = $(this).attr("value");
            if (key !== "00") {
                area_obj[key] = $(this).text();
            }
        });
        return area_obj;
    },

    /**
     * 初始省市条件数据
     * @private
     */
    _init_school_area_condition: function (year, gear) {
        var area_obj = GaoKaoSchool._get_area_arr();
        for (var area in area_obj) {
            if (area_obj.hasOwnProperty(area)) {
                var data = {
                    "action": "school_area",
                    "year": year,
                    "gear": gear,
                    "area": area,
                    "area_name": area_obj[area]
                };
                GaoKaoSchool.ajax_sync(data, function (rs) {
                    console.log(rs);
                });
            }
        }
    },

    /**
     * AJAX同步执行
     * @param data
     * @param callback
     */
    ajax_sync: function (data, callback) {
        $.ajax({
            type: "GET",
            url: Config.school_url,
            data: data,
            async: false,
            success: callback
        });
    },

    /**
     * 标记年度条件完成
     * @param year
     */
    mark_year_finish: function (year) {
        var data = {
            "action": "mark_year_finish",
            "year": year
        };
        $.get(Config.school_url, data, function (rs) {
            localStorage.removeItem(KEY_YEAR);
            console.log(rs);
            GaoKaoSchool.refresh_school_page();
        });
    },

    /**
     * 标记录取批次条件完成
     * @param year
     * @param gear
     */
    mark_gear_finish: function (year, gear) {
        var data = {
            "action": "mark_gear_finish",
            "year": year,
            "gear": gear
        };
        $.get(Config.school_url, data, function (rs) {
            localStorage.removeItem(KEY_GEAR);
            console.log(rs);
            GaoKaoSchool.refresh_school_page();
        });
    },

    /**
     * 标记省市条件完成
     * @param year
     * @param gear
     * @param area
     */
    mark_area_finish: function (year, gear, area) {
        var data = {
            "action": "mark_area_finish",
            "year": year,
            "gear": gear,
            "area": area
        };
        $.get(Config.school_url, data, function (rs) {
            localStorage.removeItem(KEY_AREA);
            console.log(rs);
            // 获取下一个省市条件数据
            GaoKaoSchool.obtain_school_area();
        });
    },

    do_post: function (eventTarget, eventArgument) {
        var theForm = document.forms['aspnetForm'];
        if (!theForm) {
            theForm = document.aspnetForm;
        }
        if (!theForm.onsubmit || (theForm.onsubmit() !== false)) {
            theForm.__EVENTTARGET.value = eventTarget;
            theForm.__EVENTARGUMENT.value = eventArgument;
            theForm.submit();
            return false;
        }
    },

    refresh_school_page: function () {
        window.location.href = 'http://218.65.240.17/Main/Luqu/YX_Sheng.aspx';
    },

    sleep: function (ms) {
        var start = new Date().getTime();
        var end = start;
        while (end < start + ms) {
            end = new Date().getTime();
        }
    }

};

$.get(Config.school_url, {action: "obtain_open_sta"}, function (open_sta) {
    if (open_sta === "1") {
        // 初始院校数据
        GaoKaoSchool.obtain_school_data();
    } else {
        var interval_id = window.setInterval(function () {
            $.get(Config.school_url, {action: "obtain_open_sta"}, function (open_sta) {
                console.log(interval_id);
                if (open_sta === "1") {
                    // 初始院校数据
                    GaoKaoSchool.obtain_school_data();
                    window.clearInterval(interval_id);
                }
            });
        }, 2000);
    }
});
